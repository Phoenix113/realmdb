package com.example.yeva.dbtask.dbHelper;

import io.realm.RealmObject;

/**
 * Created by Yeva on 22-Nov-17.
 */

public class TableUserInfo extends RealmObject {
    private String TBL_USER_NAME;
    private String TBL_USER_LAST_NAME;
    private int TBL_USER_AGE;
    private int TBL_ID;

    public String getTBL_USER_NAME() {
        return TBL_USER_NAME;
    }

    public void setTBL_USER_NAME(String TBL_USER_NAME) {
        this.TBL_USER_NAME = TBL_USER_NAME;
    }

    public void setTBL_USER_LAST_NAME(String TBL_USER_LAST_NAME) {
        this.TBL_USER_LAST_NAME = TBL_USER_LAST_NAME;
    }

    public void setTBL_USER_AGE(int TBL_USER_AGE) {
        this.TBL_USER_AGE = TBL_USER_AGE;
    }

    public String getTBL_USER_LAST_NAME() {

        return TBL_USER_LAST_NAME;
    }

    public int getTBL_USER_AGE() {
        return TBL_USER_AGE;
    }

    public int getTBL_ID() {

        return TBL_ID;
    }

    public void setTBL_ID(int TBL_ID) {
        this.TBL_ID = TBL_ID;
    }

}
