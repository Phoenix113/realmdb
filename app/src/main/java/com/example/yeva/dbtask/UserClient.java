package com.example.yeva.dbtask;

import org.json.JSONArray;

import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Yeva on 07-Dec-17.
 */

public interface UserClient {



    String KEY_Accept = "Accept: application/json";
    String KEY_Content_Type = "Content-Type: application/json";
    String KEY_Cache_Control = "Cache-Control: no-cache";

    @Headers({KEY_Accept, KEY_Content_Type, KEY_Cache_Control})
    @POST("report")
    Observable<ResponseBody> addUsers(@Body String users);
}
