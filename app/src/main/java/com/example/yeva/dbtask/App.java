package com.example.yeva.dbtask;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Yeva on 22-Nov-17.
 */

public class App extends Application {

    private RealmConfiguration sRealmConfiguration;

    @Override
    public void onCreate() {
        super.onCreate();
        // init realm db
        Realm.init(this);

        sRealmConfiguration = new RealmConfiguration.Builder()
                .schemaVersion(4)
                .build();

        Realm.setDefaultConfiguration(sRealmConfiguration);
    }

}
