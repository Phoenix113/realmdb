package com.example.yeva.dbtask;


import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by Yeva on 07-Dec-17.
 */

public class RetrofitClient {

    private static Retrofit retrofit = null;

//    private static OkHttpClient client = null;


//    public static OkHttpClient getClientHTTP() {
//        if(client == null) {
//            client = new OkHttpClient.Builder().build();
//        }
//        return client;
//    }

    public static Retrofit getClientRetrofit(String baseUrl) {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
        }
        return retrofit;
    }


}
