package com.example.yeva.dbtask.utils;

import android.app.Activity;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by Yeva on 23-Nov-17.
 */

public class Utils extends Activity {


    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }
}
