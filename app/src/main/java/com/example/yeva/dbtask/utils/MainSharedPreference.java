package com.example.yeva.dbtask.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Yeva on 24-Nov-17.
 */

public class MainSharedPreference {
    private SharedPreferences sharedPreferences;
    private static final String id = "id";

    public MainSharedPreference(Context context) {
        this.sharedPreferences = context.getSharedPreferences("My Share Preferences", Context.MODE_PRIVATE);
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    private void storeIdInSharedPreference(String key, int value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    private int getIdFromSharedPreference(String key) {
        return sharedPreferences.getInt(key, -1);
    }

    public void setIdKey(int value) {
        storeIdInSharedPreference(id, value);
    }


    public int getIdKey() {
        return getIdFromSharedPreference(id);
    }


}
