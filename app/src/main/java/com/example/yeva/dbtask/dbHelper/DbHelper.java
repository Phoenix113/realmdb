package com.example.yeva.dbtask.dbHelper;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.yeva.dbtask.Person;
import com.example.yeva.dbtask.UserClient;
import com.example.yeva.dbtask.utils.ApiUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * Created by Yeva on 23-Nov-17.
 */

public class DbHelper {

    private Realm realm;
    private static final DbHelper ourInstance = new DbHelper();
    private LinkedHashMap<String, String> hashMap = new LinkedHashMap<>();

    private JSONArray peopleJsonArray;
    private JSONObject personJsonObject;


    public static DbHelper getInstance() {
        return ourInstance;
    }

    private DbHelper() {
    }

    public void insertDataInDB(final Person person) {
        if (person != null) {
            realm = null;
            try {
                realm = Realm.getDefaultInstance();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        TableUserInfo tableUserInfo = realm.createObject(TableUserInfo.class);
                        if (!person.getName().isEmpty()) {
                            tableUserInfo.setTBL_USER_NAME(person.getName());
                        }
                        if (!person.getLastName().isEmpty()) {
                            tableUserInfo.setTBL_USER_LAST_NAME(person.getLastName());
                        }
                        if (person.getAge() != 0) {
                            tableUserInfo.setTBL_USER_AGE(person.getAge());
                        }
                        if (person.getId() != -1) {
                            tableUserInfo.setTBL_ID(person.getId());
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (realm != null) {
                    realm.close();
                }
            }
        }

    }

    public List<String> retrieveDataFromDB(final Context context) {
        final List<String> results = new ArrayList<>();
        realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<TableUserInfo> usersData = realm.where(TableUserInfo.class).findAll().sort("TBL_ID", Sort.ASCENDING);
                    if (usersData.size() != 0) {
                        for (TableUserInfo table : usersData) {
                            StringBuilder result = new StringBuilder();

                            hashMap.put("name", table.getTBL_USER_NAME());
                            result.append("Name: " + hashMap.get("name") + " ,\n");

                            hashMap.put("lastName", table.getTBL_USER_LAST_NAME());
                            result.append("Last Name: " + hashMap.get("lastName") + " ,\n");

                            hashMap.put("age", Integer.toString(table.getTBL_USER_AGE()));
                            result.append("Age: " + hashMap.get("age") + ",\n");

                            hashMap.put("id", Integer.toString(table.getTBL_ID()));
                            result.append("Id: " + hashMap.get("id") + "\n");

                            if (!result.toString().isEmpty())
                                results.add(result.toString());
                        }

                    } else {
                        Toast.makeText(context, "no Data ", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (realm != null) {
                realm.close();

            }
        }

        return results;
    }

    public void deleteAllFromDb() {
        realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<TableUserInfo> results = realm.where(TableUserInfo.class).findAll();
                    if (results.size() > 0) {
                        results.deleteAllFromRealm();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public void deleteWithId(final int id) {
        realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    TableUserInfo result = realm.where(TableUserInfo.class).equalTo("TBL_ID", id).findFirst();
                    result.deleteFromRealm();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }


    public void postDataToServer() {

        UserClient userClient = ApiUtils.getAPIService(UserClient.class);

        if (getPeopleList().length() != 0) {


            Observable<ResponseBody> responseBodyObservable = userClient.addUsers(getPeopleList());

            responseBodyObservable.subscribeOn(Schedulers.io()).subscribe(new Subscriber<ResponseBody>() {
                @Override
                public void onCompleted() {
                    Log.d("RES OK", "onResponse: ");

                }

                @Override
                public void onError(Throwable e) {
                    Log.d("RES DAMN", "onFailure: ");

                }

                @Override
                public void onNext(ResponseBody responseBody) {
                    Log.d("RES NEXT", "onResponse: ");

                }


//
//            userClient.addUsers(getPeopleList()).enqueue(new Callback<ResponseBody>() {
//                @Override
//                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//
//                    Log.d("RES OK", "onResponse: " + response.body());
//
//                }
//
//                @Override
//                public void onFailure(Call<ResponseBody> call, Throwable t) {
//                    Log.d("RES DAMN", "onFailure: " + t);
//                }
//            });
            });
        }
    }

    public String getPeopleList() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<TableUserInfo> results = realm.where(TableUserInfo.class).findAll();
                    peopleJsonArray = new JSONArray();

                    for (TableUserInfo tableUserInfo : results) {
                        try {
                            personJsonObject = new JSONObject();
                            personJsonObject.put("name", tableUserInfo.getTBL_USER_NAME());
                            personJsonObject.put("lastName", tableUserInfo.getTBL_USER_LAST_NAME());
                            personJsonObject.put("age", tableUserInfo.getTBL_USER_AGE());
                            personJsonObject.put("id", tableUserInfo.getTBL_ID());

                            peopleJsonArray.put(personJsonObject);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }


        return peopleJsonArray.toString();
    }
}
