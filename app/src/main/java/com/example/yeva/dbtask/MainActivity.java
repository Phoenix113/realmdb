package com.example.yeva.dbtask;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.yeva.dbtask.dbHelper.DbHelper;
import com.example.yeva.dbtask.utils.MainSharedPreference;

import java.util.List;


import static com.example.yeva.dbtask.utils.Utils.hideSoftKeyboard;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    String name, lastName;
    int age;
    int id = -1;
    MainSharedPreference sharedPreferences;
    private TextView resultTextView;
    private EditText nameText, lastNameText, ageText, idText;
    Button insertData, showData, deleteData, deleteAllData, postData;
    DbHelper dbHelper;
    Person person;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nameText = findViewById(R.id.name);
        lastNameText = findViewById(R.id.last_name);
        ageText = findViewById(R.id.age);
        idText = findViewById(R.id.edit_id_num);
        resultTextView = findViewById(R.id.db_text_view);
        resultTextView.setMovementMethod(new ScrollingMovementMethod());
        insertData = findViewById(R.id.insertData);
        showData = findViewById(R.id.showData);
        deleteData = findViewById(R.id.btn_delete);
        deleteAllData = findViewById(R.id.btn_delete_all);
        postData = findViewById(R.id.btn_post);
        insertData.setOnClickListener(this);
        showData.setOnClickListener(this);
        deleteData.setOnClickListener(this);
        deleteAllData.setOnClickListener(this);
        postData.setOnClickListener(this);
        dbHelper = DbHelper.getInstance();
        sharedPreferences = new MainSharedPreference(this);
    }

    public Person passDataToDB() {
        name = nameText.getText().toString();
        lastName = lastNameText.getText().toString();
        if(!name.isEmpty() && !ageText.getText().toString().isEmpty()) {
            id = sharedPreferences.getIdKey() + 1;
            sharedPreferences.setIdKey(id);
            age = Integer.parseInt(ageText.getText().toString());
            person = new Person(name, lastName, age, id);
        }

        return person;
     }

    @Override
    public void onClick(View view) {
        hideSoftKeyboard(this);
        switch (view.getId()) {
            case R.id.insertData:
                dbHelper.insertDataInDB(passDataToDB());
                break;
            case R.id.showData:
                showText();
                break;
            case R.id.btn_delete:
                // function delete
                int id = Integer.parseInt(idText.getText().toString());
                dbHelper.deleteWithId(id);
                break;
            case R.id.btn_delete_all:
                //function delete all
                sharedPreferences.setIdKey(-1);
                dbHelper.deleteAllFromDb();
                showEmptyText();
                break;
            case R.id.btn_post:
                //function post Data to server
                dbHelper.postDataToServer();

        }
    }

    public void showText() {
        List<String> allResults = dbHelper.retrieveDataFromDB(getApplicationContext());
        if (allResults.size() != 0) {
            StringBuilder s = new StringBuilder();
        for (String res : allResults) {
            s.append(res);
        }
        resultTextView.setText(s.toString());
        }
    }

    public void showEmptyText() {
        resultTextView.setText("");
    }

}
