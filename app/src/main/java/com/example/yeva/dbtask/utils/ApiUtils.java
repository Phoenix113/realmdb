package com.example.yeva.dbtask.utils;

import com.example.yeva.dbtask.RetrofitClient;

/**
 * Created by Yeva on 07-Dec-17.
 */

public class ApiUtils {

    private ApiUtils() {
    }

    public static final String BASE_URL = "https://apibeta.ifyoucan.com/";

    public static <T> T getAPIService(Class<T> tClass) {

        return RetrofitClient.getClientRetrofit(BASE_URL).create(tClass);
    }
}
